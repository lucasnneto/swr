import Vue from "vue";
import App from "./App.vue";

import VueCompositionApi from "@vue/composition-api";
import axios from "axios";
import VueAxios from "vue-axios";
import Vuex from "vuex";

Vue.use(Vuex);

Vue.use(VueAxios, axios);

Vue.use(VueCompositionApi);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  store: new Vuex.Store({
    state: {
      count: {},
    },
  }),
}).$mount("#app");
